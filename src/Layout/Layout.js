import React from "react";
import Header from "../Components/HeaderTheme/Header";

export default function Layout({ Component }) {
  return (
    <div>
      <Header />
      {<Component />}
    </div>
  );
}
