import moment from "moment/moment";
import React from "react";

export default function ItemTabMoive({ data }) {
  return (
    <div className="p-3 flex ">
      <img className="w-28 h-36 object-cover" src={data.hinhAnh} alt="" />
      <div className="flex-grow">
        <p>{data.tenPhim}</p>
        {/* map lịch chiếu */}
        <div className="grid grid-cols-3 gap-5 ">
          {data.lstLichChieuTheoPhim.slice(0, 9).map((gioChieu) => {
            return (
              <div className="p-3 rounded bg-red-500 text-white">
                {moment(gioChieu.ngayChieuGioChieu).format("DD-MM-YY ~ hh:mm")}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
