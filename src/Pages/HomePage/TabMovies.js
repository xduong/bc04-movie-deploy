import React, { useEffect } from "react";
import { Tabs } from "antd";
import { moviesServ } from "../../services/movieService";
import { useState } from "react";
import ItemTabMoive from "./ItemTabMoive";

export default function TabMovies() {
  let [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    moviesServ
      .getMovieByTheater()
      .then((res) => {
        console.log(res);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContent = () => {
    return dataMovie.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-16 h-16" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="text-left w-48">
                      <p className="truncate">{cumRap.tenCumRap}</p>
                      <p className="truncate">{cumRap.diaChi}</p>
                    </div>
                  }
                  key={index}
                >
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="scrollbar-thin scrollbar-thumb-blue-700 scrollbar-track-blue-300 overflow-y-scroll scrollbar-thumb-rounded-full scrollbar-track-rounded-full"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMoive data={phim} />;
                    })}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <div>
      <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
      ;
    </div>
  );
}
