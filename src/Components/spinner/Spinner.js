import React from "react";
import { useSelector } from "react-redux";
import { RingLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerReducer);

  //   let renderSpinner = () => {
  //     return (
  //       <div className="h-screen w-screen fixed left-0 top-0 bg-black flex justify-center items-center z-50">
  //         <RingLoader size={100} color="#ffb703" />
  //       </div>
  //     );
  //   };
  //   return <div>{isLoading && renderSpinner()}</div>;

  return isLoading ? (
    <div className="h-screen w-screen fixed left-0 top-0 bg-black flex justify-center items-center z-50">
      <RingLoader size={100} color="#ffb703" />
    </div>
  ) : (
    <></>
  );
}
