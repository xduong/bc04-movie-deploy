import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localService } from "../../services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  console.log("user", user);
  let handleLogout = () => {
    // xoá dữ liệu từ localStorage
    localService.user.remove();
    // xoá dữ liệu ở redux
    // dispatchEvent({
    //   type: SET_USER,
    //   payload: null,
    // });
    window.location.href = "/login";
  };
  let renderContent = () => {
    if (user) {
      return (
        <>
          <span>{user.hoTen}</span>
          <button
            className="border rounded"
            onClick={() => {
              handleLogout();
            }}
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button
              className="border rounded border-black px-5 py-2 hover:bg-black
            hover:text-white
            transition"
            >
              Đăng nhập
            </button>
          </NavLink>
          <button className="border rounded border-red-500 px-5 py-2">
            Đăng ký
          </button>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
