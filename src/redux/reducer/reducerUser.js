import { localService } from "../../services/localService";
import { SET_USER } from "../constants/constantUser";

let initialState = {
  userInfor: localService.user.get(),
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      state.userInfor = action.payload;
      return { ...state };

    default:
      return state;
  }
};
